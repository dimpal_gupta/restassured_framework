package TC_Reference;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package_D.Trigger_Patch_API;
import Common_Utility_Package_D.Handle_Api_logs;
import io.restassured.path.json.JsonPath;

public class Patch_TC1 extends Trigger_Patch_API {
	@Test
	public static void executor() throws IOException {
		String reqbody = patch_tc1_request();
		File Directory_name = Handle_Api_logs.creat_log_directory("Patch_TC1");
		// extract status code
		for (int i = 0; i < 5; i++) {
			int StatusCode = extract_status_Code(reqbody, patch_endpoint());
			System.out.println(StatusCode);

			if (StatusCode == 200) {
				String ResponseBody = extract_response_Body(reqbody, patch_endpoint());
				System.out.println(ResponseBody);
				Handle_Api_logs.evidence_creator(Directory_name, "Patch_TC1", patch_endpoint(), patch_tc1_request(), ResponseBody);
				validator(reqbody, ResponseBody);
				break;
			} else
				System.out.println("Declared status code not found hence retry");
		}
	}
	
	public static void validator(String reqbody, String ResponseBody) {
		// creating an object of jsonpath
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		//System.out.println("response body name is " + res_name);

		String res_job = jsp_res.getString("job");
		//System.out.println("response body job is " + res_job);

		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 10);

		// validate date
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);
		System.out.println(ExpectedDate);

		// create an object of json to parse the requestBody
		JsonPath jsp_req = new JsonPath(reqbody);
		String req_name = jsp_req.getString("name");
		System.out.println(req_name);

		String req_job = jsp_req.getString("job");
		System.out.println(req_job);

		// Validate responsebody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, ExpectedDate);
	}
}
