package TC_Reference;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package_D.Trigger_get_API;
import Common_Utility_Package_D.Handle_Get_API;
import io.restassured.path.json.JsonPath;

public class Get_TC1 extends Trigger_get_API {
	@Test
	public static void executor() throws IOException {
		//File Directory_name = Handle_Get_API.creat_log_file("Get_TC1");
		 int Status_code=0;
		for (int i = 0; i < 5; i++) {
			 Status_code = extractStatusCode(get_endpoint());
			System.out.println(Status_code);

			if (Status_code == 200) {
				String ResponseBody = extractResponseBody(get_endpoint());
				System.out.println(ResponseBody);
			//	Handle_Get_API.evidence_creater(Directory_name, "Get_TC1", get_endpoint(), ResponseBody);
				validator(ResponseBody);
				break;
			} else
				System.out.println("Declared status code not found hence retry");
		}
		Assert.assertEquals(Status_code, 200);
	}

	public static void validator(String ResponseBody) { // create an object of responsebody
		String[] Exp_id_Array = { "7", "8", "9", "10", "11", "12" };
		String [] Exp_email_Array = {"michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in"};
		String [] Exp_first_name_Array = {"Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel"};
		String [] Exp_last_name_Array = {"Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell"};
		String [] Exp_avatar_Array = {"https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg", 
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg"};
		

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_page = jsp_res.getString("page");
		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();
//		System.out.println(count);
//		System.out.println(res_data);

		for (int i = 0; i < count; i++) {
			// array of id
			String Exp_id = Exp_id_Array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			//System.out.println(res_id);
			Assert.assertEquals(res_id, Exp_id);
			
			// array of email
			String Exp_email= Exp_email_Array[i];
			String res_email = jsp_res.getString("data[" +i+ "].email");
			//System.out.println(res_email);
			Assert.assertEquals(res_email, Exp_email);
			
			// array of first name
			String Exp_first_name = Exp_first_name_Array[i];
			String res_first_name = jsp_res.getString("data[" +i+ "].first_name");
			//System.out.println(res_first_name);
			Assert.assertEquals(res_first_name, Exp_first_name);
			
			// array of last name
			String Exp_last_name = Exp_last_name_Array[i];
			String res_last_name = jsp_res.getString("data[" + i + "].last_name");
			//System.out.println(res_last_name);
			Assert.assertEquals(res_last_name, Exp_last_name);
			
			String Exp_avatar = Exp_avatar_Array[i];
			String res_avatar= jsp_res.getString("data[" +i+ "].avatar");
			System.out.println(res_avatar);
			Assert.assertEquals(res_avatar, Exp_avatar);
			
		}
	}
}
