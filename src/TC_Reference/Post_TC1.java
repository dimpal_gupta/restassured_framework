package TC_Reference;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package_D.Trigger_API_Method;
import io.restassured.path.json.JsonPath;

public class Post_TC1 extends Trigger_API_Method {
	@Test
	public static void executor() throws IOException { // extract status code
		String requestBody = post_Tc1_Request();
		//File Directory_name = Handle_Api_logs.creat_log_directory("Post_TC1"); // same class name
		// implement retry machenism
		for (int i = 0; i < 5; i++) {
			int StatusCode = extract_status_code(requestBody, post_endpoint());
		//	System.out.println("Status code " + StatusCode);

			if (StatusCode == 201) {
				String ResponseBody = extract_response_Body(requestBody, post_endpoint());
			System.out.println("response body " + ResponseBody);
				//Handle_Api_logs.evidence_creator(Directory_name, "Post_TC1", post_endpoint(), post_Tc1_Request(),
					//ResponseBody);
				validator(requestBody, ResponseBody);
				break;

			} else
				System.out.println("Desired Status code not found hence retry");
		}
	}
	
	public static void validator(String requestBody, String ResponseBody) {
		// create an object of jsonPath to parse the responsebody
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		//System.out.println(res_name);

		String res_job = jsp_res.getString("job");
		//System.out.println(res_job);
		String res_createdAt = jsp_res.getString("createdAt").substring(0, 10);

		// create an object of jsonpath to parse the requestBody
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		//System.out.println("Request body name " + req_name);

		String req_job = jsp_req.getString("job");
	//	System.out.println("Request body job " + req_job);

		// date
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);
		// System.out.println(ExpectedDate);

		// validate the responseBody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_createdAt);

	}
}