package TC_Reference;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package_D.Trigger_Put_API;
import Common_Utility_Package_D.Handle_Api_logs;
import io.restassured.path.json.JsonPath;

public class Put_TC1 extends Trigger_Put_API {
	@Test
	public static void executor() throws IOException {
		String reqBody= put_TC_Request();
		File Directory_name = Handle_Api_logs.creat_log_directory("Put_TC1");
		// extract status code
		for (int i = 0; i < 5; i++) {
			int StatusCode = extract_status_code(reqBody, put_endpoint());
			System.out.println("Status code" + StatusCode);

			if (StatusCode == 200) {
				String ResponseBody = extract_response_Body(reqBody, put_endpoint());
				//System.out.println("response body" + ResponseBody);
				Handle_Api_logs.evidence_creator(Directory_name, "Put_TC1", put_endpoint(), put_TC_Request(), ResponseBody);
				validator(reqBody, ResponseBody);
				break;

			} else
				System.out.println("Declared status code not found hence retry");
		}
	}
	
	public static void validator(String reqBody, String ResponseBody) throws IOException {
		// creating an object of jsonpath
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		System.out.println("response body " + res_name);

		String res_job = jsp_res.getString("job");
		System.out.println("response body " + res_job);
		String res_updatedAt = jsp_res.getString("updatedAt").substring(0, 10);

		// create an object of jsonpath
		JsonPath jsp_req = new JsonPath(reqBody);
		String req_name = jsp_req.getString("name");
		//System.out.println("request body " + req_name);

		String req_job = jsp_req.getString("job");
		//System.out.println("request body " + req_job);

		// date validation
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);
		System.out.println(ExpectedDate);

		// validate the responseBody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, ExpectedDate);

	}
}
