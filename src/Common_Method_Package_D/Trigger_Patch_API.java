package Common_Method_Package_D;

import static io.restassured.RestAssured.given;

import Request_Repository.Patch_Request_Repository;

public class Trigger_Patch_API extends Patch_Request_Repository {
	// extract status code
	public static int extract_status_Code(String req_Body, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(req_Body)
				.when().patch(URL)
				.then().extract().statusCode();
		
		return StatusCode;
	}

	public static String extract_response_Body(String req_Body, String URL) {
		String ResponseBody= given().header("Content-Type", "application/json").body(req_Body)
				.when().patch(URL)
				.then().extract().response().asString();
		
		return ResponseBody;
	}
}
