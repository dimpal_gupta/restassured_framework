	package Common_Utility_Package_D;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_Get_API {
// create a directory
	public static File creat_log_file(String dir_name) {
		String current_project_directory = System.getProperty("user.dir");
		System.out.println("current directory is : " + current_project_directory);
		File Log_Directory = new File(current_project_directory + "\\Api_Logs\\" + dir_name);
		deleteDirectory(Log_Directory);
		Log_Directory.mkdir();
		return Log_Directory;
	}

	private static boolean deleteDirectory(File directory) {
		boolean deleteDirectory = directory.delete();
		if (directory.exists()) {
			File[] files = directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						deleteDirectory(file);
					} else {
						file.delete();
					}
				}
			}
			deleteDirectory = directory.delete();
		} else {
			System.out.println("Directory Does not exist");
		}
		return deleteDirectory;
	}

	public static void evidence_creater(File dir_name, String file_name, String Endpoint, String ResponseBody)
			throws IOException {
		File newfile = new File(dir_name + "\\" + file_name + ".txt");
		System.out.println("New file created to save evidence : " + newfile.getName());

		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("endpont : " + Endpoint + "\n\n"); // :- \n\n :- means go to next line, 2 time enter
		datawriter.write("Response Body : \n\n " + ResponseBody); // write response body
		datawriter.close();
		System.out.println("evidence is written in file : " + newfile.getName());
	}
}
