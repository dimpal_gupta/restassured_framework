import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Common_Utility_Package_D.Excel_Data_Reader;

public class Dynamic_Driver_Class {
	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{ // class name nhi dala toh "ClassNotFoundException" : access the method which is private "SecurityException".. : 
		// Step 1 read the test cases to be executed from excel sheet

		ArrayList<String> TestCaseList = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "TestCasesToExecute",
				"TestCaseToExecute");
		System.out.println(TestCaseList);

		// how will you call the test case
		int count = TestCaseList.size(); // method to find the size of array list...and the return will be integer.
		for (int i = 1; i < count; i++) { // size of arrayList
			String TestCaseToExecute = TestCaseList.get(i);
			System.out.println("Test case which is going to be under executed is : " + TestCaseToExecute);

           // Step 2 Call the TestCaseToExecute on runtime by using java.lang.reflect package
			Class<?> TestClass = Class.forName("TC_Reference." +TestCaseToExecute);
			
			// After calling class then method should be call step 3
			//Step 3 call the execute method of the class captured in variable in test class by using Java.lang.reflect.method class
			
			Method ExecuteMethod = TestClass.getDeclaredMethod("executor");
			
			//Step 4 set the accessibility of method as true
			ExecuteMethod.setAccessible(true);
			
			// Step 5 create the instance of class capture in test class variable
			Object InstanceOfTestClass = TestClass.getDeclaredConstructor().newInstance(); // going to return the object of class

			// step 6 execute the  method captured in variable ExecuteMethod of captured  in TestClass variable
			ExecuteMethod.invoke(InstanceOfTestClass);
			System.out.println("Test class execution is completed");
			System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		}
	}
}
